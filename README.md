# Netpack
Netpack is a cross-platform, header-only library for sending both reliable and unreliable data over UDP. (to another program with netpack bound to a port).

The protocol has a Master-Slave setup where the Master is the client and the Slave is the server. The Master drives the transfer rate.

## What Netpack does
- Remains platform agnostic
- Light weight, with only a 1 byte header
- Ensure that data you want sent reliably arrives, while also allowing you to send data unreliably
- Ensure reliable data arrives in order
- Use authentication for identifying endpoints

## What Netpack does *not* do
- Compress data
- Encrypt data
- Use external libraries/dependencies aside from the standard libraries.

## Master example

```cpp
// C++11 example
#define NETPACK_BUFFER_SIZE 512

#include <iostream>
#include <thread>
#include <chrono>

#include "netpack.hpp"

bool authed = false;
bool running = true;

void on_new_connection(netpack::endpoint* end)
{
    // todo: handle call
}

void on_recieve(const char* data, netpack::endpoint* end)
{
    // todo: handle call
}

void on_response(const char* data, netpack::endpoint* end)
{
    std::cout << data << std::endl;
    running = false;
}

void on_authed(const char* data, netpack::endpoint* end)
{
    authed = true;
}

int main()
{
    netpack::auth = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx";

    netpack::on_new_connection = &on_new_connection;
    netpack::on_recieve = &on_recieve;
    netpack::on_response = &on_response;
    netpack::on_authed = &on_authed;

    // terminate if we fail to start
    if (!netpack::start()) return 0;

    netpack::endpoint* server_endpoint;
    while (!authed)
    {
        server_endpoint = netpack::connect("127.0.0.1", "8080");
        std::this_thread::sleep_for(std::chrono::seconds(1));
        netpack::recieve();
    }

    netpack::send("ping", server_endpoint);
    std::this_thread::sleep_for(std::chrono::seconds(1));

    while (running)
    {
        netpack::pull(server_endpoint);
        netpack::recieve();
    }

    netpack::close();

    return 0;
}
```

## Protocol
Really for my own reference, the protocol works as such:

### Connecting (Netpack to Netpack)
1) Connect -> Send an auth packet (packet with nothing but auth token) to the target netpack client
2) Recieve -> Receive acknowledging "a " packet from target netpack. if not, send again

## TODO
- make close also clean up memory
- support bluetooth connections
- support IPv6
