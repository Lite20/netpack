#pragma once

#include <string>
#include <cstring>
#include <iostream>
#include <stdio.h>
#include <unordered_map>
#include <sstream>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <thread>

#define NETPACK_AUTH_TOKEN_LENGTH 36

#ifdef _WIN32
#define _WIN32_WINNT 0x600

#pragma comment(lib, "ws2_32.lib")

#include <winsock2.h>
#include <ws2tcpip.h>

#ifdef _WIN32
#include <windows.h>
#endif

#define NETPACK_SOCKTYPE SOCKET
#define NETPACK_CLOSE_CALL(x) closesocket(x)
#else
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <memory.h>
#include <ifaddrs.h>
#include <net/if.h>

#define NETPACK_SOCKTYPE int
#define NETPACK_CLOSE_CALL(x) close(x)
#define SOCKET_ERROR -1
#define INVALID_SOCKET -1
#endif

namespace netpack
{

struct endpoint;

int fail(int);

void clean();

void udp_send(const char, endpoint*);

// used by master to send
void send(std::string, endpoint*);

// used by master to request response
void pull(endpoint*);

std::string get_ip_and_port(struct sockaddr_in*);

endpoint* connect(const char*, unsigned short int);

void recieve();

bool start();

/*
    +--------------------------------------------------------------------------------------------+
    |                                     DEFINITION SECTION                                     |
    +--------------------------------------------------------------------------------------------+
    All the definitions for the components above lay below. Be sure to define "NETPACK_DEFINITION"
    *one* time in your source so that these are available to your code.
*/

#ifdef NETPACK_DEFINITION

#ifdef _WIN32
WSADATA wsa;
#endif

NETPACK_SOCKTYPE sock;

struct sockaddr_in remaddr;

const char *auth;

socklen_t addrlen = sizeof(remaddr);

char in_buff[NETPACK_BUFFER_SIZE];

short unsigned int port = 0;

std::unordered_map<std::string, endpoint *> connections;

void (*on_new_connection)(endpoint *);
void (*on_recieve)(const char *, endpoint *);
void (*on_response)(const char *, endpoint *);
void (*on_authed)(endpoint *);

struct endpoint
{
    char offset = 0;

    char *auth_token;
    char *packets[4];

    struct sockaddr_in addr;

    std::ostringstream out;

    // used by slave to send
    void pack(std::string data)
    {
        out << ':' << data;
    }

    void cycle()
    {
        offset++;
        if (offset == 4)
            offset = 0;
    }

    void serialize()
    {
        std::string package = out.str();
        strcpy(packets[static_cast<int>(offset)], package.c_str());

        cycle();

        out.str("");
        out << offset;
    }

    endpoint(struct sockaddr_in &paddr)
    {
        addr = paddr;
        out << offset;
    }

    endpoint(const char *ip, unsigned short int port)
    {
        addr.sin_family = AF_INET;
        addr.sin_port = htons(port);
        inet_pton(AF_INET, ip, &(addr.sin_addr));
    }
};

int fail(int code)
{
#ifdef _WIN32
    return WSAGetLastError();
#else
    return code;
#endif
}

void clean()
{
    NETPACK_CLOSE_CALL(sock);

#ifdef _WIN32
    WSACleanup();
#endif
}

void udp_send(const char *data, endpoint *end)
{
    sendto(
        sock, data, strlen(data), 0, (sockaddr *)&(end->addr), sizeof(end->addr));
}

// used by master to send
void send(std::string string_data, endpoint *end)
{
    // +1 for header, and +1 for terminator
    int size = string_data.length() + NETPACK_AUTH_TOKEN_LENGTH + 2;
    char *data = new char[size];
    data[0] = '-';
    memcpy(&data[1], auth, 36);
    memcpy(&data[36], string_data.c_str(), string_data.length());
    data[size - 1] = '\0';

    udp_send(const_cast<char *>(data), end);

    delete data;
}

// used by master to request response
void pull(endpoint *end)
{
    // +1 for header, and +1 for terminator
    char *data = new char[NETPACK_AUTH_TOKEN_LENGTH + 2];
    char header = (end->offset) | 0x80;

    strcpy(&data[1], auth);
    data[0] = header;
    data[NETPACK_AUTH_TOKEN_LENGTH + 1] = '\0';

    udp_send(const_cast<char *>(data), end);

    delete data;
}

std::string get_ip_and_port(struct sockaddr_in *s)
{
    // put ip at the start
    char buf[INET_ADDRSTRLEN];
    const char *ip_part = inet_ntop(AF_INET, &(s->sin_addr), buf, sizeof(buf));
    std::string ip = std::string(ip_part);

    // add port to the end
    uint16_t port = htons(s->sin_port);
    return ip + std::to_string(port);
}

endpoint *connect(const char *ip, unsigned short int port)
{
    endpoint *end = new endpoint(ip, port);

    // get id (ip:port without the colon)
    std::string string_id = ip + std::to_string(port);

    // TODO check if overwriting old connection and clean up
    connections[string_id] = end;

    // authenticate
    udp_send(auth, end);

    return end;
}

void recieve()
{
    int recvlen = recvfrom(
        sock, in_buff, NETPACK_BUFFER_SIZE, 0,
        (struct sockaddr *)&remaddr, &addrlen);

    // get id (ip:port without the colon)
    // add 2 for port which is a short (2 bytes)
    std::string id = get_ip_and_port(&remaddr);

    // new connection
    if (recvlen == NETPACK_AUTH_TOKEN_LENGTH)
    {
        // put auth token into endpoint struct
        endpoint *end = new endpoint(remaddr);
        end->auth_token = new char[strlen(in_buff)];
        strcpy(end->auth_token, in_buff);

        std::pair<std::unordered_map<std::string, endpoint *>::iterator, bool> ret;
        ret = connections.insert(std::pair<std::string, endpoint *>(id, end));
        if (ret.second == false)
        {
            // clean up, remove, and re-add to map
            delete ret.first->second;
            connections.erase(ret.first);
            connections[id] = end;
        }

        // call new socket handler
        const char *authflag = "a\0";
        udp_send(authflag, end);
        on_new_connection(end);
    }
    // existing connection
    else if (recvlen > NETPACK_AUTH_TOKEN_LENGTH)
    {
        // extract auth token. +1 for null terminator character
        char auth_token[NETPACK_AUTH_TOKEN_LENGTH + 1];
        memcpy(auth_token, &in_buff[1], NETPACK_AUTH_TOKEN_LENGTH);
        auth_token[NETPACK_AUTH_TOKEN_LENGTH] = '\0';

        endpoint *end = connections.at(id);
        if (strcmp(end->auth_token, auth_token) != 0)
        {
            // invalid auth. reject
            return;
        }

        // fetch request
        if (in_buff[0] & 0x80)
        {
            unsigned char pack_id = in_buff[0] & 0x3;
            // if requesting current packet, package contents and cycle offset
            if (end->offset == pack_id)
                end->serialize();
            udp_send(end->packets[pack_id], end);
        }

        // data sent to us
        else
        {
            // response
            if (in_buff[0] == end->offset)
            {
                end->cycle();
                on_response(const_cast<char *>(in_buff), end);
            }
            // inbound data
            else
            {
                std::cout << "inbound data" << std::endl;

                // make new char* to hold data and call inbound handler.
                char *data = new char[recvlen - NETPACK_AUTH_TOKEN_LENGTH];
                // + 1 as first char is request netpack header
                memcpy(
                    data,
                    &in_buff[NETPACK_AUTH_TOKEN_LENGTH + 1],
                    recvlen - NETPACK_AUTH_TOKEN_LENGTH);

                on_recieve(const_cast<char *>(data), end);

                delete data;
            }
        }
    }
    else if (recvlen == 1 && in_buff[0] == 'a')
    {
        on_authed(connections.at(id));
    }
}

bool start()
{
#ifdef _WIN32
    // Initialise winsock
    if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0)
    {
        std::cout << "Failed to init winsock. " << fail(0) << std::endl;
        return false;
    }

#endif

    if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) == INVALID_SOCKET)
    {
        std::cout << "Failed to build socket. " << fail(1) << std::endl;
        return false;
    }

    struct sockaddr_in addrListen;
    addrListen.sin_family = AF_INET;
    addrListen.sin_addr.s_addr = INADDR_ANY;
    addrListen.sin_port = htons(port);
    if (bind(sock, (struct sockaddr *)&addrListen, sizeof(addrListen)) == SOCKET_ERROR)
    {
        std::cout << "Failed to bind to port. " << fail(2) << std::endl;
        return false;
    }

    socklen_t addrsize = sizeof(addrListen);
    if (getsockname(sock, (struct sockaddr *)&addrListen, &addrsize) == -1)
    {
        std::cout << "Failed to get assigned address " << fail(3) << std::endl;
        return false;
    }

    port = ntohs(addrListen.sin_port);

    return true;
}
#endif
} // namespace netpack
